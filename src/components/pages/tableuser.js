// eslint-disable-next-line import/no-unresolved
import { supabase } from '$src/supabaseClient';

// eslint-disable-next-line import/no-unresolved
import { invalid } from '..pages/_404';

/** @type {import('./$types').Actions} */
export const actions = {
	postTiket: async ({ request }) => {
		const data = await request.formData();
		const problem = data.get('Subject');
		const detail = data.get('Pesan');
		const { error } = await supabase.from('ticket').insert([{ problem, detail }]);

		if (error) {
			return invalid(500, error);
		}
	},
}



/** @type {import('./$types').PageLoad} */
export async function load() {
    
	let { data} = await supabase
		.from('ticket')
		.select('*')
		.order('id', { ascending: false });
    return {
            id: data.id,
            problem: data.problem,
            detail: data.detail,
            attachment: data.attachment,
            created_at: data.created_at
        };
    
}
